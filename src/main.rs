// Copyright (c) 2016 nail developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! nailed - Nail Daemon
#![cfg_attr(feature="clippy", feature(plugin))]
#![cfg_attr(feature="clippy", plugin(clippy))]
#![cfg_attr(feature="clippy", deny(clippy, clippy_pedantic))]
#![deny(missing_docs)]
#[macro_use]
extern crate clap;
extern crate libnail;
extern crate mio;
extern crate rand;

mod error;

use clap::{App, Arg, ArgMatches};
use error::NailedErr;
use libnail::{NailMessage, NailMessageHandler, Server};
use mio::{EventLoop, Ipv4Addr, Sender, Token};
use mio::tcp::TcpListener;
use rand::Rng;
use std::collections::HashMap;
use std::io::{self, Write};
use std::net::{SocketAddr, SocketAddrV4};
use std::process;
use std::sync::mpsc::channel;
use std::thread;
use std::time::Duration;

type NailedResult<T> = Result<T, NailedErr>;

const SSH2: &'static str = "SSH-2.0-";

struct SshHandler {
    client_versions: HashMap<Token, String>,
}

impl NailMessageHandler for SshHandler {
    fn new() -> SshHandler {
        SshHandler { client_versions: HashMap::new() }
    }

    fn handle(&self, channel: Sender<NailMessage>, message: NailMessage) {
        thread::spawn(move || {
            write!(io::stdout(), "Read '{}'\n", message).expect("Unable to write to stdout!");
            let mut rng = rand::thread_rng();
            let n = rng.gen_range(0, 10);
            thread::sleep(Duration::from_secs(n));
            channel.send(message).expect("Unable to notify event loop");
        });
    }
}

fn event_loop(matches: ArgMatches) -> NailedResult<()> {
    let mut retries: i8 = try!(matches.value_of("retries").unwrap_or("0").parse());
    let sleep_duration =
        Duration::from_millis(try!(matches.value_of("sleep").unwrap_or("1000").parse()));
    let ip: Ipv4Addr = try!(matches.value_of("addr").unwrap_or("0.0.0.0").parse());
    let port: u16 = try!(matches.value_of("port").unwrap_or("32276").parse());
    let mut event_loop = try!(EventLoop::new());
    let addr = SocketAddr::V4(SocketAddrV4::new(ip, port));
    let socket = try!(TcpListener::bind(&addr));
    let handler = SshHandler::new();
    let mut daemon = Server::new(socket, handler);
    daemon.register(&mut event_loop);
    let (tx, rx) = channel();

    thread::spawn(move || {
        loop {
            match event_loop.run(&mut daemon) {
                Ok(_) => {
                    if retries > 0 {
                        retries -= 1;
                        thread::sleep(sleep_duration);
                    } else if retries == -1 {
                        thread::sleep(sleep_duration);
                    } else {
                        break;
                    }
                }
                Err(e) => {
                    writeln!(io::stderr(), "{}", e).expect("Unable to write to stderr!");
                    break;
                }
            }
        }

        tx.send(0).expect("Unable to send 0");
    });

    let _ = rx.recv();
    Ok(())
}

fn run() -> i32 {
    let matches = App::new("nailed")
        .version(crate_version!())
        .author("Jason Ozias <jason.g.ozias@gmail.com>")
        .about("nail daemon for handling nail requests")
        .arg(Arg::with_name("retries")
            .short("r")
            .long("retries")
            .value_name("RETRIES")
            .help("Specify a the number of retries when restarting the event loop")
            .takes_value(true))
        .arg(Arg::with_name("sleep")
            .short("s")
            .long("sleep")
            .value_name("SLEEP")
            .help("Specify a the number of ms to sleep when restarting the event loop")
            .takes_value(true))
        .arg(Arg::with_name("address")
            .short("a")
            .long("addr")
            .value_name("ADDRESS")
            .help("Specify a the IP address for the daemon to listen on.")
            .takes_value(true))
        .arg(Arg::with_name("port")
            .short("p")
            .long("port")
            .value_name("PORT")
            .help("Specify a the port for the daemon to listen on.")
            .takes_value(true))
        .arg(Arg::with_name("verbose")
            .short("v")
            .multiple(true)
            .help("Set the verbosity level (more v's = more verbose)"))
        .get_matches();

    match event_loop(matches) {
        Ok(_) => 0,
        Err(e) => {
            writeln!(io::stderr(), "{}", e).expect("Unable to write to stderr!");
            1
        }
    }
}

fn main() {
    process::exit(run());
}
