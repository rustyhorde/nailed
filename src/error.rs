//! Error Implementation for nailed.
use std::error;
use std::fmt;
use std::io;

use self::NailedErr::{AddrParseError, Io, ParseIntError};

/// Errors thrown by libnail.
#[derive(Debug)]
pub enum NailedErr {
    // AddrParseError wrapper
    AddrParseError(::std::net::AddrParseError),
    /// io::Error wrapper
    Io(io::Error),
    /// std::num::ParseIntError wrapper
    ParseIntError(::std::num::ParseIntError),
}

impl fmt::Display for NailedErr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            AddrParseError(ref err) => err.fmt(f),
            Io(ref err) => err.fmt(f),
            ParseIntError(ref err) => err.fmt(f),
        }
    }
}

impl error::Error for NailedErr {
    fn description(&self) -> &str {
        match *self {
            AddrParseError(ref err) => err.description(),
            Io(ref err) => err.description(),
            ParseIntError(ref err) => err.description(),
        }
    }

    fn cause(&self) -> Option<&error::Error> {
        match *self {
            AddrParseError(ref err) => Some(err),
            Io(ref err) => Some(err),
            ParseIntError(ref err) => Some(err),
        }
    }
}

impl From<io::Error> for NailedErr {
    fn from(err: io::Error) -> NailedErr {
        Io(err)
    }
}

impl From<::std::num::ParseIntError> for NailedErr {
    fn from(err: ::std::num::ParseIntError) -> NailedErr {
        ParseIntError(err)
    }
}

impl From<::std::net::AddrParseError> for NailedErr {
    fn from(err: ::std::net::AddrParseError) -> NailedErr {
        AddrParseError(err)
    }
}
